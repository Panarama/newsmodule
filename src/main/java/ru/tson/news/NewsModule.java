package ru.tson.news;

import org.springframework.stereotype.Component;
import ru.core.tson.module.BaseIRModule;
import ru.core.tson.module.EventManager;

import javax.annotation.PostConstruct;

@Component
public class NewsModule extends BaseIRModule {
    public String getName() {
        return "News Module";
    }

    public void run() {
        System.out.println("Run "+ getName());
    }

    public void destroy() throws Exception {
        System.out.println("Destroy "+ getName());
    }

    @PostConstruct
    public void init(){
        events = new EventManager("update");
    }
    public void afterPropertiesSet() throws Exception {

    }
}
