package ru.tson.news;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan(basePackageClasses = {NewsModule.class})
//@EntityScan(basePackages = "ru.core.module")
public class NewsConfiguration {
}
