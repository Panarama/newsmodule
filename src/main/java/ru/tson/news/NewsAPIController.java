package ru.tson.news;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;

@RestController
@RequestMapping("/news")
public class NewsAPIController {

    @Autowired
    NewsModule module;

    @RequestMapping(value = "/new", method = RequestMethod.POST)
    String get(@RequestBody News[] news){
        news = Arrays.asList(news).stream().sorted((new1, new2)->new2.getPriority()-new1.getPriority()).toArray(size -> new News[size]);
        module.event("update", true, news);
        return "{\"result\": \"ok\"}";
    }

}
