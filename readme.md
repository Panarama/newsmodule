# Модуль NewsModule
NewsModule - это модуль предоставляющий API для добавления новых новостных записей (например для бегущей строки) для модульной системы https://bitbucket.org/Panarama/tsoncore


### Добавление / Обновление новостей

| URL | DATA | RESPONCE
| ------ | ------ | ------
| **[POST]** http://[host]/news/new | ``` [{"text":"Hello4","priority":14,"date":12121234353,"source":"Kontora 1"},{...}] ``` | ```{"result:" "OK"} ```

### Формат JSON
| Ключ | Описание
| ------ | ------ 
| text | Текст новости
| priority | Приоритет новости
| date | дата публикации
| source | источник новости